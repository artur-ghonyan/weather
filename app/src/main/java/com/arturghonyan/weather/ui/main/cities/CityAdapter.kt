package com.arturghonyan.weather.ui.main.cities

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.arturghonyan.weather.database.room.city.City
import com.arturghonyan.weather.databinding.CityItemBinding
import com.arturghonyan.weather.helpers.ItemMoveCallback
import java.util.*

class CityAdapter(private val cityListener: CityListener) :
    RecyclerView.Adapter<CityAdapter.CityViewHolder>(),
    ItemMoveCallback.ItemTouchHelperContract {

    private var data: MutableList<City>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CityViewHolder {
        return CityViewHolder(
            CityItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: CityViewHolder, position: Int) {
        holder.onBind(position)
    }

    override fun getItemCount(): Int {
        return data?.size ?: 0
    }

    fun setData(newData: MutableList<City>) {
        data = newData
        notifyDataSetChanged()
    }

    fun addData(city: City) {
        val addCity = data?.contains(city) ?: false
        if (!addCity) {
            data?.add(city)
            notifyDataSetChanged()
        }
    }

    fun getData(): MutableList<City> {
        return data ?: mutableListOf()
    }

    inner class CityViewHolder(private val itemBinding: CityItemBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun onBind(position: Int) {
            val city: City? = data?.elementAt(position)
            itemBinding.labelCityName.text = city?.name ?: ""

            itemView.setOnClickListener {
                if (city != null) {
                    cityListener.onLongClickListener(city)
                }
            }
        }
    }

    override fun onRowMoved(fromPosition: Int, toPosition: Int) {
        data?.get(fromPosition)?.position = toPosition
        data?.get(toPosition)?.position = fromPosition

        Collections.swap(data, fromPosition, toPosition)
        notifyItemMoved(fromPosition, toPosition)

        cityListener.onDrag()
    }
}