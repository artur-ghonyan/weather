package com.arturghonyan.weather.ui.main.cities

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.arturghonyan.weather.database.room.city.City
import com.arturghonyan.weather.database.room.weather.WeatherInfo
import com.arturghonyan.weather.helpers.AuthSingleton
import com.arturghonyan.weather.network.APIClient
import com.arturghonyan.weather.network.APIInterface
import com.arturghonyan.weather.utils.Constants
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CityViewModel : ViewModel() {

    private var cityList: MutableList<City>? = null

    val setCityList: MutableLiveData<MutableList<City>> by lazy {
        MutableLiveData<MutableList<City>>()
    }

    val addCity: MutableLiveData<City> by lazy {
        MutableLiveData<City>()
    }

    init {
        getCityList()
    }

    private fun getCityList() = GlobalScope.launch {
        cityList = AuthSingleton.getDatabase()?.cityDao()?.getAll()
        setCityList.postValue(cityList)
    }

    fun hasCity(cityName: String): Boolean {
        val city = cityList?.find { it.name.equals(cityName, ignoreCase = true) }
        return city != null
    }

    fun requestWeather(newCity: String) {
        val apiInterface = APIClient.instance().create(APIInterface::class.java)
        apiInterface?.getCurrentWeather(newCity, Constants.METRIC, Constants.API_KEY)
            ?.enqueue(object : Callback<WeatherInfo?> {
                override fun onResponse(
                    call: Call<WeatherInfo?>,
                    response: Response<WeatherInfo?>
                ) {
                    val weatherInfo = response.body()

                    if (weatherInfo != null) {
                        val city = City(newCity, cityList?.size ?: 0)
                        saveCity(city)
                        saveWeather(weatherInfo)

                        addCity.postValue(city)
                    } else {
                        addCity.postValue(null)
                    }
                }

                override fun onFailure(call: Call<WeatherInfo?>, t: Throwable) {
                    t.printStackTrace()
                    addCity.postValue(null)
                }
            })
    }

    fun saveCity(city: City) = GlobalScope.launch {
        cityList?.add(city)
        AuthSingleton.getDatabase()?.cityDao()?.insert(city)
    }

    fun deleteCity(city: City) = GlobalScope.launch {
        cityList?.remove(city)
        AuthSingleton.getDatabase()?.cityDao()?.delete(city)
        AuthSingleton.getDatabase()?.weatherDao()?.deleteByCity(city.name)

        setCityList.postValue(cityList)
    }

    fun saveWeather(weatherInfo: WeatherInfo) = GlobalScope.launch {
        AuthSingleton.getDatabase()?.weatherDao()?.insert(weatherInfo)
    }

    fun updateCityList(newList: MutableList<City>) = GlobalScope.launch {
        AuthSingleton.getDatabase()?.cityDao()?.clearTable()
        AuthSingleton.getDatabase()?.cityDao()?.insert(newList)
    }
}