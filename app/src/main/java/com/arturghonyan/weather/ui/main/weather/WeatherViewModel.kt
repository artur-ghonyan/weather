package com.arturghonyan.weather.ui.main.weather

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.arturghonyan.weather.R
import com.arturghonyan.weather.database.room.city.City
import com.arturghonyan.weather.database.room.weather.WeatherInfo
import com.arturghonyan.weather.helpers.AuthSingleton
import com.arturghonyan.weather.network.APIClient
import com.arturghonyan.weather.network.APIInterface
import com.arturghonyan.weather.utils.Constants
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class WeatherViewModel(val app: Application) : AndroidViewModel(app) {

    companion object {
        private const val MAX_ITEM_COUNT = 10
    }

    var page: Int = 1
    private var fromDB = false

    private var apiInterface: APIInterface? = null

    private var cityList: MutableList<City>? = mutableListOf()
    private var dbWeatherList: MutableList<WeatherInfo?>? = mutableListOf()
    private var weatherListForRV: MutableList<WeatherInfo?>? = mutableListOf()

    val setWeatherInfoList: MutableLiveData<MutableList<WeatherInfo?>> by lazy {
        MutableLiveData<MutableList<WeatherInfo?>>()
    }

    val newWeatherItem: MutableLiveData<WeatherInfo> by lazy {
        MutableLiveData<WeatherInfo>()
    }

    val updateList: MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>()
    }

    init {
        apiInterface = APIClient.instance().create(APIInterface::class.java)
    }

    fun getLists() = GlobalScope.launch {
        cityList = AuthSingleton.getDatabase()?.cityDao()?.getAll()
        dbWeatherList = AuthSingleton.getDatabase()?.weatherDao()?.getAll()

        updateList.postValue(true)
    }

    fun requestFromApi() {
        page = 1
        weatherListForRV?.clear()
        fromDB = false
        requestWeatherFromApi()
    }

    fun readFromDB() {
        page = 1
        weatherListForRV?.clear()
        fromDB = true

        if (!dbWeatherList.isNullOrEmpty()) {
            updateRV()
        } else {
            Toast.makeText(
                app,
                app.resources.getString(R.string.please_connect_internet),
                Toast.LENGTH_LONG
            ).show()
        }
    }

    fun getWeatherListSize(): Int {
        return dbWeatherList?.size ?: 0
    }

    fun setPage() {
        page++
        if (fromDB) {
            updateRV()
        } else {
            requestWeatherFromApi()
        }
    }

    private fun requestWeatherFromApi() {
        val start = MAX_ITEM_COUNT * (page - 1)
        val end = MAX_ITEM_COUNT * page
        val count = cityList?.size ?: 0

        var sendResultNull = true

        for (i in start until count) {
            if (i < end) {
                sendResultNull = false
                requestWeather(cityList?.get(i)?.name ?: "")
            } else {
                break
            }
        }
        if (sendResultNull) {
            newWeatherItem.value = null
        }
    }

    private fun requestWeather(selectedCity: String) {
        apiInterface?.getCurrentWeather(selectedCity, Constants.METRIC, Constants.API_KEY)
            ?.enqueue(object : Callback<WeatherInfo?> {
                override fun onResponse(
                    call: Call<WeatherInfo?>,
                    response: Response<WeatherInfo?>
                ) {
                    val weatherInfo: WeatherInfo? = response.body()

                    if (weatherInfo != null) {
                        saveWeather(weatherInfo)
                    }
                    newWeatherItem.value = weatherInfo
                }

                override fun onFailure(call: Call<WeatherInfo?>, t: Throwable) {
                    t.printStackTrace()
                    newWeatherItem.value = null
                }
            })
    }

    private fun updateRV() {
        weatherListForRV?.clear()
        val end = MAX_ITEM_COUNT * page
        val count = dbWeatherList?.size ?: 0

        for (i in 0 until count) {
            if (i < end) {
                weatherListForRV?.add(dbWeatherList?.get(i))
            } else {
                break
            }
        }
        setWeatherInfoList.value = weatherListForRV
    }

    fun saveWeather(weatherInfo: WeatherInfo) = GlobalScope.launch {
        AuthSingleton.getDatabase()?.weatherDao()?.insert(weatherInfo)
    }
}