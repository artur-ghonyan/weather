package com.arturghonyan.weather.ui.main.cities

import com.arturghonyan.weather.database.room.city.City

interface CityListener {
    fun onLongClickListener(city: City)
    fun onDrag()
}