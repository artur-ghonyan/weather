package com.arturghonyan.weather.ui.main.weather

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.arturghonyan.weather.R
import com.arturghonyan.weather.database.room.weather.WeatherInfo
import com.arturghonyan.weather.databinding.WeatherItemBinding
import com.arturghonyan.weather.utils.decimal

class WeatherAdapter : RecyclerView.Adapter<WeatherAdapter.WeatherViewHolder>() {

    private var data: MutableList<WeatherInfo?> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeatherViewHolder {
        return WeatherViewHolder(
            WeatherItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: WeatherViewHolder, position: Int) {
        holder.onBind(position)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun setData(newData: MutableList<WeatherInfo?>) {
        data = newData
        notifyDataSetChanged()
    }

    fun addData(weatherInfo: WeatherInfo) {
        val noAdd = data.contains(weatherInfo)
        if (!noAdd) {
            data.add(weatherInfo)
            notifyDataSetChanged()
        }
    }

    fun getDataSize(): Int {
        return data.size
    }

    inner class WeatherViewHolder(private val itemBinding: WeatherItemBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun onBind(position: Int) {
            val weatherInfo: WeatherInfo? = data.elementAt(position)
            itemBinding.labelCityName.text = weatherInfo?.name ?: ""
            itemBinding.labelWeather.text =
                String.format(
                    itemView.context.getString(R.string.percent),
                    weatherInfo?.main?.temp?.decimal()
                )

            itemBinding.labelPressure.text =
                String.format(
                    itemView.context.getString(R.string.pressure),
                    weatherInfo?.main?.pressure?.toInt()
                )

            itemBinding.labelWindSpeed.text =
                String.format(
                    itemView.context.getString(R.string.wind_speed),
                    weatherInfo?.wind?.speed?.toInt()
                )

            itemBinding.labelHumidity.text =
                String.format(
                    itemView.context.getString(R.string.humidity),
                    weatherInfo?.main?.humidity?.toInt()
                )
        }
    }
}