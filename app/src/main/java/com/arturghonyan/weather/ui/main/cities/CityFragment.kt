package com.arturghonyan.weather.ui.main.cities

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import com.arturghonyan.weather.R
import com.arturghonyan.weather.database.room.city.City
import com.arturghonyan.weather.databinding.FragmentCityBinding
import com.arturghonyan.weather.helpers.ItemMoveCallback
import com.arturghonyan.weather.ui.BaseFragment
import com.arturghonyan.weather.utils.Utils

class CityFragment : BaseFragment() {

    private var fragmentCityBinding: FragmentCityBinding? = null
    private val binding get() = fragmentCityBinding

    private var cityViewModel: CityViewModel? = null
    private var adapter: CityAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        fragmentCityBinding = FragmentCityBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = CityAdapter(object : CityListener {
            override fun onLongClickListener(city: City) {
                val message =
                    requireContext().getString(R.string.are_you_sure_you_want_to_delete_this_city)
                        .let { String.format(it, city.name) }

                AlertDialog.Builder(requireContext())
                    .setTitle(getString(R.string.delete_city))
                    .setMessage(message)
                    .setPositiveButton(getString(R.string.yes)) { dialog, _ ->
                        cityViewModel?.deleteCity(city)
                        dialog.dismiss()
                    }
                    .setNegativeButton(getString(R.string.no), null)
                    .show()
            }

            override fun onDrag() {
                cityViewModel?.updateCityList(adapter?.getData() ?: mutableListOf())
            }
        })

        val callback: ItemTouchHelper.Callback = ItemMoveCallback(adapter!!)
        val touchHelper = ItemTouchHelper(callback)
        touchHelper.attachToRecyclerView(binding?.cityRecyclerView)
        binding?.cityRecyclerView?.adapter = adapter

        cityViewModel = ViewModelProvider(this).get(CityViewModel::class.java)

        cityViewModel?.setCityList?.observe(viewLifecycleOwner) {
            adapter?.setData(it)
        }

        cityViewModel?.addCity?.observe(viewLifecycleOwner) {
            binding?.progressBarCity?.isVisible = false

            binding?.actionCheckCityAvailable?.isEnabled = true
            if (it != null) {
                binding?.actionAddCity?.setText("")
                adapter?.addData(it)
            } else {
                Toast.makeText(
                    requireContext(),
                    getString(R.string.city_not_found),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

        binding?.actionCheckCityAvailable?.setOnClickListener {
            val cityName = binding?.actionAddCity?.text?.toString()

            if (!cityName.isNullOrEmpty()) {
                val hasCity = cityViewModel?.hasCity(cityName) ?: false
                if (!hasCity) {
                    binding?.actionCheckCityAvailable?.isEnabled = false
                    binding?.progressBarCity?.isVisible = true
                    Utils.hideKeyboard(requireActivity())
                    cityViewModel?.requestWeather(cityName)
                } else {
                    Toast.makeText(
                        requireContext(),
                        getString(R.string.city_already_exists),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        fragmentCityBinding = null
    }
}