package com.arturghonyan.weather.ui.main.weather

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.arturghonyan.weather.R
import com.arturghonyan.weather.databinding.FragmentWeatherBinding
import com.arturghonyan.weather.ui.BaseFragment

class WeatherFragment : BaseFragment() {

    private var fragmentWeatherBinding: FragmentWeatherBinding? = null
    private val binding get() = fragmentWeatherBinding

    private var connectivityManager: ConnectivityManager? = null

    private var weatherViewModel: WeatherViewModel? = null
    private var adapter: WeatherAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        fragmentWeatherBinding = FragmentWeatherBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)

        connectivityManager =
            requireActivity().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        adapter = WeatherAdapter()
        binding?.weatherRecyclerView?.adapter = adapter

        weatherViewModel = ViewModelProvider(this).get(WeatherViewModel::class.java)
        weatherViewModel?.getLists()
        weatherViewModel?.page = 1

        weatherViewModel?.updateList?.observe(viewLifecycleOwner, {
            if (it) {
                if (internetOnAvailable()) {
                    weatherViewModel?.requestFromApi()
                } else {
                    weatherViewModel?.readFromDB()
                }
            }
        })

        weatherViewModel?.setWeatherInfoList?.observe(viewLifecycleOwner, {
            binding?.progressBarWeather?.isVisible = false
            binding?.actionAddCity?.isVisible = it.isNullOrEmpty()
            if (!it.isNullOrEmpty()) {
                adapter?.setData(it)
            }
        })

        weatherViewModel?.newWeatherItem?.observe(viewLifecycleOwner, {
            binding?.progressBarWeather?.isVisible = false
            if (it != null) {
                binding?.actionAddCity?.isVisible = false
                adapter?.addData(it)
            }
        })

        binding?.weatherRecyclerView?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)

                val totalItemCount = recyclerView.layoutManager!!.itemCount

                val item =
                    (recyclerView.layoutManager as LinearLayoutManager).findLastVisibleItemPosition()

                if (totalItemCount - 1 == item) {
                    val noSetPage = weatherViewModel?.getWeatherListSize() == adapter?.getDataSize()

                    if (!noSetPage) {
                        binding?.progressBarWeather?.isVisible = true
                        weatherViewModel?.setPage()
                    } else {
                        binding?.progressBarWeather?.isVisible = false
                    }
                }
            }
        })

        binding?.actionAddCity?.setOnClickListener {
            goToCitiesList()
        }
    }

    override fun onStart() {
        super.onStart()
        connectivityManager?.registerNetworkCallback(
            NetworkRequest.Builder().addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
                .build(), networkCallback
        )
    }

    private fun internetOnAvailable(): Boolean {
        val capabilities =
            connectivityManager?.getNetworkCapabilities(connectivityManager?.activeNetwork)
        if (capabilities != null) {
            when {
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> return true
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> return true
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> return true
            }
        }
        return false
    }

    override fun onStop() {
        super.onStop()
        connectivityManager?.unregisterNetworkCallback(networkCallback)
    }

    private val networkCallback = object : ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network) {
            super.onAvailable(network)
            Log.d("InternetLog", getString(R.string.internet_connected))
        }

        override fun onLost(network: Network) {
            super.onLost(network)
            Log.d("InternetLog", getString(R.string.internet_disconnected))
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_weather_fragment, menu)
        return super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.city -> goToCitiesList()
        }
        return true
    }

    private fun goToCitiesList() {
        navigateToFragment(
            R.id.navigation_weather_fragment,
            R.id.action_weatherFragment_to_citiesFragment,
            null
        )
    }

    override fun onDestroyView() {
        super.onDestroyView()
        fragmentWeatherBinding = null
    }
}