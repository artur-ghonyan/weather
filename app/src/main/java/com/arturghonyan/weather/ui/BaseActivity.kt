package com.arturghonyan.weather.ui

import androidx.appcompat.app.AppCompatActivity

open class BaseActivity : AppCompatActivity()