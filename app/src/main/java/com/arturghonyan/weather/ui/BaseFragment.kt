package com.arturghonyan.weather.ui

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation

open class BaseFragment : Fragment() {

    private var navController: NavController? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = Navigation.findNavController(view)
    }

    fun navigateToFragment(check: Int, action: Int, bundle: Bundle? = null) {
        if (navController?.currentDestination?.id == check)
            navController?.navigate(action, bundle)
    }
}