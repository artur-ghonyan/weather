package com.arturghonyan.weather.ui.main

import android.os.Bundle
import com.arturghonyan.weather.databinding.ActivityMainBinding
import com.arturghonyan.weather.ui.BaseActivity

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}