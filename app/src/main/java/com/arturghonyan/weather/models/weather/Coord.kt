package com.arturghonyan.weather.models.weather

import com.google.gson.annotations.SerializedName

data class Coord(
    @SerializedName("lon")
    var longitude: String? = null,

    @SerializedName("lat")
    var latitude: String? = null
)
