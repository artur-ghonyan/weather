package com.arturghonyan.weather.models.weather

data class Wind(
    var speed: Double? = null,
    var deg: Double? = null
)
