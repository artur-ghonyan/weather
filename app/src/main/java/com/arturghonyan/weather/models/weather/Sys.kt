package com.arturghonyan.weather.models.weather

data class Sys(
    var type: Double? = null,
    var country: String? = null,
    var sunrise: Double? = null,
    var sunset: Double? = null,
)
