package com.arturghonyan.weather.models.weather

import com.google.gson.annotations.SerializedName

data class Main(
    var temp: Double? = null,
    @SerializedName("feels_like")
    var feelsLike: Double? = null,
    @SerializedName("temp_min")
    var tempMin: Double? = null,
    @SerializedName("temp_max")
    var tempMax: Double? = null,
    var pressure: Double? = null,
    var humidity: Double? = null
)