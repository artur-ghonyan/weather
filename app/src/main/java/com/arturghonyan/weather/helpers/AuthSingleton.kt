package com.arturghonyan.weather.helpers

import com.arturghonyan.weather.database.room.AppDatabase

object AuthSingleton {
    private var appDatabase: AppDatabase? = null

    fun setDatabase(appDatabase: AppDatabase?) {
        AuthSingleton.appDatabase = appDatabase
    }

    fun getDatabase(): AppDatabase? {
        return appDatabase
    }
}