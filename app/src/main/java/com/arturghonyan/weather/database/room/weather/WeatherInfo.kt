package com.arturghonyan.weather.database.room.weather

import androidx.annotation.NonNull
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.arturghonyan.weather.models.weather.Coord
import com.arturghonyan.weather.models.weather.*
import com.google.gson.annotations.SerializedName

@Entity
data class WeatherInfo(
    @PrimaryKey
    @NonNull
    var id: Long = 0,

    @Embedded
    @SerializedName("coord")
    var coordinate: Coord? = null,

    @Embedded
    var main: Main? = null,
    var visibility: Double? = null,

    @Embedded
    var wind: Wind? = null,

    @Embedded
    var clouds: Clouds? = null,
    var dt: Double? = null,

    @Embedded
    @SerializedName("sys")
    var system: Sys? = null,

    var timezone: Long? = null,
    var name: String? = null,
    var cod: Double? = null
)
