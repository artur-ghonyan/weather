package com.arturghonyan.weather.database.room.city

import androidx.room.*

@Dao
interface CityDao {

    @Query("SELECT * FROM city ORDER BY position")
    fun getAll(): MutableList<City>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(city: City)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(cityList: MutableList<City>)

    @Delete
    fun delete(city: City)

    @Query("DELETE FROM city")
    fun clearTable()
}