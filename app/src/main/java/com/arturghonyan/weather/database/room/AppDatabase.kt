package com.arturghonyan.weather.database.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.arturghonyan.weather.database.room.city.City
import com.arturghonyan.weather.database.room.city.CityDao
import com.arturghonyan.weather.database.room.weather.WeatherInfo
import com.arturghonyan.weather.database.room.weather.WeatherInfoDao

@Database(entities = [City::class, WeatherInfo::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun cityDao(): CityDao

    abstract fun weatherDao(): WeatherInfoDao
}