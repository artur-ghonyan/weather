package com.arturghonyan.weather.database.room.city

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class City(
    @PrimaryKey
    @NonNull
    var name: String = "",

    @NonNull
    var position: Int = 0
)