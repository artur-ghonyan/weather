package com.arturghonyan.weather.database.room.weather

import androidx.room.*

@Dao
interface WeatherInfoDao {

    @Query("SELECT weatherinfo.* FROM city INNER JOIN weatherinfo ON city.name == weatherinfo.name ORDER BY city.position")
    fun getAll(): MutableList<WeatherInfo?>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(weatherInfo: WeatherInfo)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(weatherInfo: MutableList<WeatherInfo>)

    @Delete
    fun delete(weatherInfo: WeatherInfo)

    @Query("DELETE FROM weatherinfo WHERE name = :cityName")
    fun deleteByCity(cityName: String)

    @Query("DELETE FROM weatherinfo")
    fun clearTable()
}