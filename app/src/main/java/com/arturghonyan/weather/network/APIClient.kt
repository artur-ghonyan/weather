package com.arturghonyan.weather.network

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class APIClient {

    companion object {

        private const val BASE_URL = "https://api.openweathermap.org/"

        private var retrofit: Retrofit? = null

        fun instance(): Retrofit {
            retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            return retrofit!!
        }
    }
}