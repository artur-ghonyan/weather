package com.arturghonyan.weather.network

import com.arturghonyan.weather.database.room.weather.WeatherInfo
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface APIInterface {

    @GET("data/2.5/weather?")
    fun getCurrentWeather(
        @Query("q") city: String?,
        @Query("units") units: String?,
        @Query("appid") apiKey: String?
    ): Call<WeatherInfo?>?
}