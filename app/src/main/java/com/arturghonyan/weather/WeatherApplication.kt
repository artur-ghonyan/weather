package com.arturghonyan.weather

import androidx.multidex.MultiDexApplication
import androidx.room.Room
import com.arturghonyan.weather.database.room.AppDatabase
import com.arturghonyan.weather.helpers.AuthSingleton
import com.arturghonyan.weather.utils.Constants

class WeatherApplication : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()

        val database =
            Room.databaseBuilder(this, AppDatabase::class.java, Constants.DATABASE_NAME).build()
        AuthSingleton.setDatabase(database)
    }
}