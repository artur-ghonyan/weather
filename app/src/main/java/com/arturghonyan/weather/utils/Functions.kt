package com.arturghonyan.weather.utils

import java.text.DecimalFormat

fun Double.decimal(): String {
    val decimalFormat = DecimalFormat("#.#")

    return if (decimalFormat.format(this).toDouble() % 1 == 0.toDouble()) {
        (decimalFormat.format(this).toDouble() + "0.0".toDouble()).toString()
    } else {
        decimalFormat.format(this)
    }
}