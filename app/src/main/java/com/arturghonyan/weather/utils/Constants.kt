package com.arturghonyan.weather.utils

object Constants {

    const val DATABASE_NAME = "room_database_weather"

    // API key, which we get when register on OpenWeatherMap platform
    const val API_KEY = "103d81484acba06de623a689412ea453"

    // We need to use this as a parameter for the request in order to get temperature in Celsius
    const val METRIC = "metric"
}