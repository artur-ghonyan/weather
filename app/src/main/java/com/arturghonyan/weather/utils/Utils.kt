package com.arturghonyan.weather.utils

import android.app.Activity
import android.content.Context
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager

class Utils {
    companion object {
        fun hideKeyboard(activity: Activity) {
            try {
                activity.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
                if (activity.currentFocus != null && activity.currentFocus!!
                        .windowToken != null
                ) {
                    (activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(
                        activity.currentFocus!!.windowToken, 0
                    )
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}